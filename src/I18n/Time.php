<?php
namespace Riddlemd\Tools\I18n;

use Cake\I18n\Time as BaseTime;

class Time extends BaseTime
{
    /* DateTime format strings */
    public const DATETIME_SQL_DATETIME =    'Y-m-d H:i:s';
    public const DATETIME_SQL_DATE =        'Y-m-d';
    public const DATETIME_SQL_TIME =        'H:i:s';
    public const DATETIME_INPUT_DATETIME =  'Y-m-d\TH:i:s';
    public const DATETIME_INPUT_DATE =      'Y-m-d';
    public const DATETIME_INPUT_TIME =      'H:i:s';

    /* I18n date/time format strings */
    public const I18N_SQL_DATETIME =        'yyyy-MM-dd HH:mm:ss';
    public const I18N_SQL_DATE =            'yyyy-MM-dd';
    public const I18N_SQL_TIME =            'HH:mm:ss';
    public const I18N_INPUT_DATETIME =      'yyyy-MM-dd\'T\'HH:mm:ss';
    public const I18N_INPUT_DATE =          'yyyy-MM-dd';
    public const I18N_INPUT_TIME =          'HH:mm:ss';

    public function getStartOfMonth() : Time
    {
        return $this->copy()->day(1)->hour(0)->minute(0)->second(0);
    }

    public function getEndOfMonth() : Time
    {
        return $this->getStartOfMonth()->modify('+1 month')->modify('-1 second');
    }
}