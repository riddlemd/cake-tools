<?php
namespace Riddlemd\Tools\View\Helper;

use Cake\View\Helper\FormHelper as BaseHelper;
use Cake\View\View;
use Cake\Chronos\ChronosInterface;
use Cake\View\Form\EntityContext;
use Riddlemd\Tools\I18n\Time;

class FormHelper extends BaseHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->setTemplates([
            'hiddenBlock' => '<x-form-data>{{content}}</x-form-data>',
            'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>',
            'checkboxWrapper' => '<div class="{{namespace}}checkbox">{{label}}</div>',
            'error' => '<div class="{{namespace}}error-message">{{content}}</div>',
            'inputContainer' => '<div class="{{namespace}}input {{namespace}}input_{{type}} {{required}}">{{content}}</div>',
            'inputContainerError' => '<div class="{{namespace}}input {{namespace}}input_{{type}} {{required}} {{namespace}}input_error">{{content}}{{error}}</div>',
        ]);
    }

    protected function _inputContainerTemplate($options)
    {
        $inputContainerTemplate = $options['options']['type'] . 'Container' . $options['errorSuffix'];
        if (!$this->templater()->get($inputContainerTemplate)) {
            $inputContainerTemplate = 'inputContainer' . $options['errorSuffix'];
        }
        return $this->formatTemplate($inputContainerTemplate, [
            'content' => $options['content'],
            'error' => $options['error'],
            'required' => $options['options']['required'] ? ' required' : '',
            'type' => $options['options']['type'],
            'namespace' => $this->getConfig('namespace') ? "{$this->getConfig('namespace')}__" : null,
            'templateVars' => isset($options['options']['templateVars']) ? $options['options']['templateVars'] : []
        ]);
    }

    protected function _initInputField($fieldName, $options = [])
    {
        $options = parent::_initInputField($fieldName, $options);

        if(isset($options['queryAssignable']) && $options['queryAssignable'] && $this->getView()->getRequest()->getQuery($fieldName) !== null)
            $options['val'] = $this->getView()->getRequest()->getQuery($fieldName);

        $context = $this->_getContext();
        if(isset($options['type']) && is_a($context, EntityContext::class))
        {
            $entity = $context->entity();

            switch($options['type'])
            {
                case 'password':
                case 'radio':
                case 'hidden':
                    break;
                case 'time':
                    if($entity->getOriginal($fieldName)) $options['data-original-value'] = $entity->getOriginal($fieldName)->I18nFormat(Time::I18N_INPUT_TIME, $this->getView()->getRequest()->getSession()->read('Auth.User')->timezone);
                    break;
                case 'date':
                    if($entity->getOriginal($fieldName))  $options['data-original-value'] = $entity->getOriginal($fieldName)->I18nFormat(Time::I18N_INPUT_DATE, $this->getView()->getRequest()->getSession()->read('Auth.User')->timezone);
                    break;
                case 'datetime':
                case 'datetime-local':
                    if($entity->getOriginal($fieldName)) $options['data-original-value'] = $entity->getOriginal($fieldName)->I18nFormat(Time::I18N_INPUT_DATETIME, $this->getView()->getRequest()->getSession()->read('Auth.User')->timezone);
                    break;
                case 'tel':
                    $options['val'] = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $options['val']);
                    $options['data-original-value'] = $entity->getOriginal($fieldName);
                    break;
                case 'fauxInput':
                    break;
                default:
                    $originalValue = $entity->getOriginal($fieldName);
                    $options['data-original-value'] = is_object($originalValue) || is_array($originalValue) ? json_encode($originalValue) : $originalValue;
                    break;
            }
        }
        
        return $options;
    }

    public function create($context = null, array $options = [])
    {
        if(!empty($options['namespace']))
        {
            $this->setConfig('namespace', $options['namespace']);
            unset($options['namespace']);
        }
        if(!empty($this->getConfig('namespace')))
        {
            $options['class'] = (!empty($options['class']) ? $options['class'] . ' ' : '') . $this->getConfig('namespace') . '__form';
        }
        return parent::create($context, $options);
    }
    
    // Override CakePHP's date input formatter since HTML5 provides one now. Also handles converting times to local user timezone.
    public function date($fieldName, array $options = [])
    {
        $options = $this->_initInputField($fieldName, $options);
        
        $timezone = new \DateTimeZone($this->getView()->getRequest()->getSession()->check('Auth.User.timezone') ? $this->getView()->getRequest()->getSession()->read('Auth.User.timezone') : 'UTC'); 
        
        if($options['val'] instanceof ChronosInterface)
        {
            $options['val'] = $options['val']->I18nFormat(Time::I18N_INPUT_DATE, $timezone);
        }
        else
        {
            $options['val'] = explode(' ', $options['val'])[0]; // Workaround for Cake autofilling dates in wrong format when reloading form after errors
        }
        
        return $this->widget($fieldName, $options);
    }
    
    // Override CakePHP's time input formatter since HTML5 provides one now. Also handles converting times to local user timezone.
    public function time($fieldName, array $options = [])
    {
        $options = $this->_initInputField($fieldName, $options);
        
        $timezone = new \DateTimeZone($this->getView()->getRequest()->getSession()->check('Auth.User.timezone') ? $this->getView()->getRequest()->getSession()->read('Auth.User.timezone') : 'UTC'); 
        
        if($options['val'] instanceof ChronosInterface)
        {
            $options['val'] = $options['val']->I18nFormat(Time::I18N_INPUT_TIME, $timezone);
        }
        
        return $this->widget($fieldName, $options);
    }
     
    // Override CakePHP's datetime input formatter since HTML5 provides one now. Also handles converting times to local user timezone.
    public function dateTime($fieldName, array $options = [])
    {
        $options = $this->_initInputField($fieldName, $options);
        
        $timezone = $this->getView()->getRequest()->getSession()->read('Auth.User.timezone') ?? 'UTC';

        $options['type'] = 'datetime-local';
        if($options['val'] instanceof ChronosInterface)
        {
            $options['val'] = $options['val']->I18nFormat(Time::I18N_INPUT_DATETIME, $timezone);
        }

        return $this->widget($fieldName, $options);
    }
}