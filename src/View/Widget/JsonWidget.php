<?php
namespace Riddlemd\Tools\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\WidgetInterface;
use Cake\I18n\Number;

class JsonWidget implements WidgetInterface
{
    protected $_templates;

    public function __construct($templates)
    {
        $this->_templates = $templates;
    }

    public function render(array $data, ContextInterface $context)
    {
        $value = json_encode($data['val'], JSON_PRETTY_PRINT);
        if($value == '[]' || $value == '{}') $value = 'null'; // Set empty arrays and objects to null
            
        return $this->_templates->format('textarea', [
            'name' => $data['name'],
            'attrs' => $this->_templates->formatAttributes($data, ['name', 'val']),
            'value' => $value
        ]);
    }

    public function secureFields(array $data)
    {
        return [];
    }
}