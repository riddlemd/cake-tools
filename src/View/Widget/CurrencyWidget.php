<?php
namespace Riddlemd\Tools\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\WidgetInterface;
use Cake\I18n\Number;

class CurrencyWidget implements WidgetInterface
{
    protected $_templates;

    public function __construct($templates)
    {
        $this->_templates = $templates;
    }

    public function render(array $data, ContextInterface $context)
    {        
        $data['type'] = 'number';
        $data['step'] = '0.01';
        $data['default'] = '0.00';

        if(array_key_exists('val', $data) && $data['val'] !== null && $data['val'] !== '')
        {
            $data['value'] = Number::precision($data['val'], 2, [
                'pattern' => '0.00',
            ]);
            unset($data['val']);
        }

        return $this->_templates->format('input', [
            'name' => $data['name'],
            'type' => $data['type'],
            'templateVars' => $data['templateVars'],
            'attrs' => $this->_templates->formatAttributes(
                $data,
                ['name', 'type']
            ),
        ]);
    }
    
    public function secureFields(array $data)
    {
        return [];
    }
}