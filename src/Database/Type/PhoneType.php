<?php
namespace Riddlemd\Tools\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use Cake\Database\TypeInterface;
use Cake\Utility\Security;
use PDO;

class PhoneType extends Type implements TypeInterface
{
    public function toDatabase($value, Driver $driver)
    {
        return preg_replace('/[^0-9]+/', '' , $value);
    }

    public function toPHP($value, Driver $driver)
    {
        if(!empty($value))
        {
            return preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $value);
        }
        return '';
    }
}