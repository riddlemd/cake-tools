<?php
namespace Riddlemd\Tools\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use Cake\Utility\Security;

class CryptedType extends Type
{
    public function toDatabase($value, Driver $driver)
    {
        return Security::encrypt($value, Security::salt());
    }

    public function toPHP($value, Driver $driver)
    {
        if($value)
        {
            return Security::decrypt($value, Security::salt());
        }
        return '';
    }
}