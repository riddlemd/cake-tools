<?php
namespace Riddlemd\Tools\Core;

trait TraitEventsTrait
{
    private function __traitConstruct(...$args)
    {
        foreach(preg_grep('/__.+TraitConstruct/', get_class_methods($this)) as $methodName)
            call_user_func_array(self::class . '::' . $methodName, $args);
    }

    private function __traitDestruct(...$args)
    {
        foreach(preg_grep('/__.+TraitDestruct/', get_class_methods($this)) as $methodName)
            call_user_func_array(self::class . '::' . $methodName, $args);
    }
}