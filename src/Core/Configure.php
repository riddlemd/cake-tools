<?php
namespace Riddlemd\Tools\Core;

use Cake\Utility\Hash;

class Configure extends \Cake\Core\Configure
{
    // Works like Configure::write, except it will not overwrite keys with existing values.
    public static function softWrite($keys, $value = null)
    {
        if(!is_array($keys))
            $keys = [$keys => $value];

        $keys = Hash::flatten($keys);

        foreach($keys as $key => $value)
        {
            if(!Configure::check($key))
                Configure::write($key, $value);
        }
    }
}