<?php
namespace Riddlemd\Tools\Controller;

use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Utility\Inflector;

trait DeleteTrait
{
    public function delete()
    {
        $this->getRequest()->allowMethod(['patch', 'post', 'put']);

        $this->getEventManager()->on('Controller.beforeDelete', [$this, 'beforeDelete']);
        $this->getEventManager()->on('Controller.afterDelete', [$this, 'afterDelete']);
        
        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entitiesName = $this->getPrimaryModelInfo()['entitiesName'];
        $primaryKey = $this->$modelName->getPrimaryKey();
        $methodName = 'findBy' . Inflector::camelize($primaryKey);
        $data = $this->getRequest()->getData();
        $hasErrors = false;
        $results = [];

        if(empty($data[$entitiesName])) $data = [$entitiesName => [$data]];

        foreach($data[$entitiesName] as $entityData)
        {
            if(!isset($entityData[$primaryKey])) continue;

            $result = [
                $primaryKey => $entityData[$primaryKey],
                'deleted' => false
            ];

            $entity = $this->$modelName->$methodName($entityData[$primaryKey])->first();
            if($entity)
            {
                $beforeDeleteEvent = $this->dispatchEvent('Controller.beforeDelete', compact('entity'));
                
                if($beforeDeleteEvent->result !== false)
                {
                    $deleteResult = $this->$modelName->delete($entity, ['user' => $this->Auth->getUser()]);
                    $this->dispatchEvent('Controller.afterDelete', ['entity' => $entity, 'deleted' => $deleteResult]);

                    $result['deleted'] = true;
                }
            }

            if(!$result['deleted']) $hasErrors = true;

            $results[] = $result;
        }

        if(!$hasErrors)
        {
            $message = count($data) > 1 ? "All {$modelName} Deleted" : Inflector::singularize($modelName) . " Deleted";
            $this->Flash->success(__($message));

            return $this->redirect(['action' => 'search']);
        }
        else
        {
            $this->Flash->error(__("Some {$modelName} could not be Deleted"));
            return $this->redirect($this->referer());
        }


        $this->set($entitiesName, $results);
    }

    public function beforeDelete(Event $event, Entity $entity)
    {
        // Do nothing...
    }

    public function afterDelete(Event $event, Entity $entity, bool $deleted)
    {
        // Do nothing...
    }
}