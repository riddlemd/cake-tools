<?php
namespace Riddlemd\Tools\Controller;

trait CreateAsEditAliasTrait {
    public function create()
    {
        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entity = $this->$modelName->newEntity();

        $this->edit($entity);
        $this->viewBuilder()->setTemplate('edit');
    }
}