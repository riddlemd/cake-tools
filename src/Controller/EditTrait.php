<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;

trait EditTrait {
    public function edit($needle=null) {
        $this->getEventManager()->on('Controller.' . 'beforeEdit', [$this, 'beforeEdit']);
        $this->getEventManager()->on('Controller.' . 'afterEdit', [$this, 'afterEdit']);

        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entityName = $this->getPrimaryModelInfo()['entityName'];
        $primaryKey = $this->$modelName->getPrimaryKey();
        $data = $this->request->getData();

        $entity = null;
        $entityQuery = null;

        if(is_a($needle, 'Cake\ORM\Entity')) {
            $entity = $needle;
            $needle = $entity->id;
        } else {
            if(substr($needle, 0, 2) === '0x') $needle = intval(substr($needle, 2), 16);
            $methodName = 'findBy' . Inflector::camelize($primaryKey);
            $entityQuery = $this->$modelName->$methodName($needle);
        }

        $associations = null;

        $event = $this->dispatchEvent('Controller.beforeEdit', [
            'entity' => $entity,
            'query' => $entityQuery,
            'data' => &$data,
            'associations' => &$associations
        ]);
        
        if(!is_a($entity, 'Cake\ORM\Entity'))
        {
            if($associations) $entityQuery->contain($associations);
            $entity = $entityQuery->first();
        }

        if(empty($entity))
            throw new \Cake\Http\Exception\NotFoundException('Record not found');

        if($event->result !== false)
        {
            if($this->getRequest()->is(['post', 'put', 'patch']))
            {
                if(!$entity->isNew()) {
                    $data['id'] = $needle;
                }

                $associated = [];
                if(is_array($associations))
                {
                    foreach($associations as $associationKey => $associationValue)
                    {
                        $associated[] = is_string($associationValue) ? $associationValue : $associationKey;
                    }
                }

                $entity = $this->$modelName->patchEntity($entity, $data, [
                    'associated' => $associated,
                    'user' => $this->Auth->user()
                ]);

                if($this->$modelName->save($entity, ['associated' => $associated, 'user' => $this->Auth->user()]))
                {
                    $this->dispatchEvent('Controller.afterEdit', [
                        'entity' => $entity
                    ]);

                    $this->Flash->success(__('Record has been saved'));

                    return $this->redirect([
                        'action' => 'edit',
                        $entity->$primaryKey
                    ]);
                } else {
                    $this->Flash->error(__('Could not save Record'));
                }
            }
        }

        $this->set($entityName, $entity);
    }

    public function beforeEdit(Event $event, ?Entity $entity, ?Query $query, Array &$data, ?Array &$associations = null)
    {
        // Do nothing...
    }

    public function afterEdit(Event $event, Entity $entity)
    {
        // Do nothing...
    }
}