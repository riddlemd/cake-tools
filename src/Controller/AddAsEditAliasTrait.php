<?php
namespace Riddlemd\Tools\Controller;

trait AddAsEditAliasTrait {
    public function add()
    {
        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entity = $this->$modelName->newEntity();

        $this->edit($entity);
        $this->viewBuilder()->setTemplate('edit');
    }
}