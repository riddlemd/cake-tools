<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;

trait CopyTrait {
    public function copy($id=null)
    {
        $this->getRequest()->allowMethod(['patch', 'post', 'put']);

        $this->getEventManager()->on('Controller.' . 'beforeCopy', [$this, 'beforeCopy']);
        $this->getEventManager()->on('Controller.' . 'afterCopy', [$this, 'afterCopy']);
        
        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $primaryKey = $this->$modelName->getPrimaryKey();
        $data = $this->getRequest()->getData();

        if(!empty($data[$primaryKey]))
        {
            $methodName = 'findBy' . Inflector::camelize($primaryKey);

            $originalEntity = $this->$modelName->$methodName($data[$primaryKey])
                ->first();

            if(empty($originalEntity))
                throw new \Cake\Http\Exception\NotFoundException('Record not found');
            
            $newEntity = clone $originalEntity;

            $newEntity->$primaryKey = null;
            $newEntity->isNew(true);

            // Find unique & primary keys and set them null so entity can be copied
            $schema = $this->$modelName->getSchema();
            $constraints = $schema->constraints();
            foreach($newEntity->toArray() as $column => $value)
            {
                if(in_array($column, $constraints))
                {
                    $newEntity->$column = null;
                }
            }
            
            // Find belongs to association and nulls them.
            $associations = $this->$modelName->associations();
            foreach($associations as $association)
            {
                $foreignKey = $association->getForeignKey();
                $columnSchema = $schema->getColumn($foreignKey);

                if(!$columnSchema['null']) continue;

                $newEntity->$foreignKey = null;
            }

            $beforeCopyEvent = $this->dispatchEvent('Controller.beforeCopy', [$originalEntity, $newEntity]);

            if($beforeCopyEvent->result !== false && $this->$modelName->save($newEntity))
            {
                $this->dispatchEvent('Controller.afterCopy', [$newEntity]);

                $this->Flash->success(__('Record has been copied'));
                
                return $this->redirect([
                    'action' => 'edit',
                    $newEntity->$primaryKey
                ]);
            }
            

            $this->Flash->error(__('Could not copy Record'));
        }

        return $this->redirect($this->referer());
    }

    public function beforeCopy(Event $event, Entity $originalEntity, Entity $newEntity)
    {
        // Do nothing...
    }

    public function afterCopy(Event $event, Entity $entity)
    {
        // Do nothing...
    }
}