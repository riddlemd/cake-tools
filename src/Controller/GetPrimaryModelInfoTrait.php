<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;

trait GetPrimaryModelInfoTrait
{
    private $_primaryModelInfo = null;
    public function getPrimaryModelInfo()
    {
        if(!$this->_primaryModelInfo)
        {
            $modelName = explode('.', $this->modelClass);
            $modelName = end($modelName);
            $entityName = Inflector::variable($modelName);
            $this->_primaryModelInfo = [
                'modelName' => $modelName,
                'entityName' => Inflector::singularize($entityName),
                'entitiesName' => Inflector::pluralize($entityName)
            ];
        }
        return $this->_primaryModelInfo;
    }
}