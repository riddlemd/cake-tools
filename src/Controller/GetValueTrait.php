<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;

trait GetValueTrait
{
    public function getValue($id=null,$field=null)
    {
        if(substr($id, 0, 2) === '0x') $id = intval(substr($id, 2), 16);

        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $fieldCamal = Inflector::camelize($field);
        $field = Inflector::underscore($field);
        $primaryKey = $this->$modelName->getPrimaryKey();

        $methodName = 'findBy' . Inflector::camelize($primaryKey);
        $entity = $this->$modelName->$methodName($id)
            ->select([
                $field
            ])
            ->first();

        if(empty($entity))
            throw new \Cake\Http\Exception\NotFoundException('Record not found');
        
        $this->set('value', $entity->$field);
        $this->set('_serialize', ['value']);
    }
}