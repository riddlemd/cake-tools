<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Query;
use Riddlemd\Tools\Model\Table\RequestQueryableTableInterface;

trait SearchTrait {
    public function search()
    {
        $this->getEventManager()->on('Controller.' . 'beforeSearch', [$this, 'beforeSearch']);

        $request = $this->getRequest();
        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entitiesName = $this->getPrimaryModelInfo()['entitiesName'];
        $primaryKey = $this->$modelName->getPrimaryKey();

        $requestQuery = $request->getQuery();

        $query = is_a($this->$modelName, RequestQueryableTableInterface::class) ? $this->$modelName->generateQueryFromKeyValuePairs($requestQuery) : $this->$modelName->find();

        if($tags = $request->getParam('tags'))
        {
            $tags = explode(',', $tags);
            $this->$modelName->filterQueryByKeywords($query, $tags);
        }

        $event = $this->dispatchEvent('Controller.beforeSearch', ['query' => &$query]);

        if(strpos($query->sql(), ' ORDER BY ') === false && $this->request->getQuery('sort') === null) $query->order(["{$modelName}.{$primaryKey}" => 'DESC']);

        $entities = $this->paginate($query);

        $paginationInformation = $this->request->getParam("paging.{$modelName}");
        $this->set([
            $entitiesName => $entities,
            'count' => $paginationInformation['count'],
            'pageCount' => $paginationInformation['pageCount']
        ]);
    }

    public function beforeSearch(Event $event, Query $query)
    {
        // Do nothing...
    }
}