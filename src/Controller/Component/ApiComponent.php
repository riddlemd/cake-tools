<?php
namespace Riddlemd\Tools\Controller\Component;

use Cake\Controller\Component as BaseComponent;
use Cake\Event\Event;

class ApiComponent extends BaseComponent
{
    public function initialize(array $config = [])
    {
        parent::initialize($config);

        $request = $this->getController()->getRequest();
        $request->addDetector('api', function($request) {
            return $request->is('json') || $request->is('xml');
        });
    }

    public function beforeRender(Event $event)
    {
        $controller = $this->getController();
        $request = $controller->getRequest();

        if($request->is('api'))
        {
            $session = $request->getSession();

            $flashMessages = [];
            $flashes = $session->read('Flash.flash');

            if(!empty($flashes))
            {
                foreach($flashes as $flash)
                {
                    $type = substr($flash['element'], 6);

                    if(!isset($flashMessages[$type])) { $flashMessages[$type] = []; }

                    $flashMessages[$type][] = $flash['message'];
                }
            }

            $session->delete('Flash.flash');

            foreach($flashMessages as $type => $typeMessages)
            {
                $controller->set($type, $typeMessages);
            }
        }
    }
}