<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;

trait ViewTrait {
    public function view($needle=null)
    {
        if(substr($needle, 0, 2) === '0x') $needle = intval(substr($needle, 2), 16);
        
        $this->getEventManager()->on('Controller.' . 'beforeView', [$this, 'beforeView']);

        $modelName = $this->getPrimaryModelInfo()['modelName'];
        $entityName = $this->getPrimaryModelInfo()['entityName'];
        $primaryKey = $this->$modelName->getPrimaryKey();

        $entity = null;
        $methodName = 'findBy' . Inflector::camelize($primaryKey);
        $query = $this->$modelName->$methodName($needle);

        $event = $this->dispatchEvent('Controller.beforeView', [
            'entity' => &$entity,
            'query' => &$query
        ]);

        if(!is_a($entity, 'Cake\ORM\Entity'))
            $entity = $query->first();

        if(empty($entity))
            throw new \Cake\Http\Exception\NotFoundException('Record not found');

        $this->set($entityName, $entity);
    }

    public function beforeView(Event $event, ?Entity &$entity, Query &$query)
    {
        // Do nothing...
    }
}