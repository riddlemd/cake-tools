<?php
namespace Riddlemd\Tools\Controller;

use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;

trait ToggleTrait {
    public function toggle()
    {
        $this->getEventManager()->on('Controller.' . 'beforeToggle', [$this, 'beforeToggle']);
        $this->getEventManager()->on('Controller.' . 'afterToggle', [$this, 'afterToggle']);

        if($this->getRequest()->is(['patch', 'post', 'put']))
        {
            $modelName = $this->getPrimaryModelInfo()['modelName'];
            $primaryKey = $this->$modelName->getPrimaryKey();
            $data = $this->getRequest()->getData();

            if(!empty($data[$primaryKey]) && !empty($data['column']))
            {
                $primaryKeyValue = $data[$primaryKey];
                $column = $data['column'];

                $modelName = $this->getPrimaryModelInfo()['modelName'];
                $methodName = 'findBy' . Inflector::camelize($primaryKey);

                $schema = $this->$modelName->schema();
                $type = $schema->column($column)['type'] ?? null;

                if($type === 'boolean')
                {
                    $query = $this->$modelName->$methodName($primaryKeyValue);
                    $entity = $query->first();

                    $beforeToggleEvent = $this->dispatchEvent('Controller.beforeToggle', [
                        'entity' => $entity,
                        'column' => $column
                    ]);

                    if($beforeToggleEvent->result !== false)
                    {
                        $entity->$column = !$entity->$column;

                        $this->$modelName->save($entity, ['user' => $this->Auth->user()]);

                        $afterToggleEvent = $this->dispatchEvent('Controller.afterToggle', [
                            'entity' => $entity,
                            'column' => $column
                        ]);
                    }

                    $this->set([
                        $primaryKey => $entity->$primaryKey,
                        'column' => $column,
                        'value' => $entity->$column
                    ]);

                    return $this->redirect($this->referer());
                }
            }
        }
    }

    public function beforeToggle(Event $event, Entity $entity, string $column)
    {
        // Do nothing...
    }

    public function afterToggle(Event $event, Entity $entity, string $column)
    {
        // Do nothing...
    }
}