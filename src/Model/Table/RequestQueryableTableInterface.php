<?php
namespace Riddlemd\Tools\Model\Table;

use Cake\ORM\Query;

interface RequestQueryableTableInterface
{
    public function getGlobalSearchFields() : array;
    public function setGlobalSearchFields(array $fields) : self;
    public function getPresentationFields() : array;
    public function setPresentationFields(array $fields) : self;
    public function generateQueryFromKeyValuePairs(array $requestQuery, array $options = []) : Query;
}