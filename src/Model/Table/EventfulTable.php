<?php
namespace Riddlemd\Tools\Model\Table;

use Cake\ORM\Table as BaseTable;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;

abstract class EventfulTable extends BaseTable
{
    use \Riddlemd\Tools\Core\TraitEventsTrait;

    protected $_eventfulEventMap = [
        'Model.beforePatchEntity' => 'beforePatchEntity',
        'Model.afterPatchEntity' => 'afterPatchEntity',

        'Controller.beforeCopy' => 'beforeCopy',
        'Controller.afterCopy' => 'afterCopy',

        'Controller.beforeDelete' => 'beforeDelete',
        'Controller.afterDelete' => 'afterDelete',

        'Controller.beforeEdit' => 'beforeEdit',
        'Controller.afterEdit' => 'afterEdit,',
    ];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->__traitConstruct($config);
    }

    public function __destruct()
    {
        $this->__traitDestruct();
    }

    public function initialize(array $config)
    {
        parent::initialize($config);

        if(empty($this->_globalSearchFields) || !is_array($this->_globalSearchFields))
            $this->_globalSearchFields = [$this->getDisplayField()];
    }

    public function implementedEvents()
    {
        $events =  parent::implementedEvents();

        foreach ($this->_eventfulEventMap as $event => $method) {
            if (!method_exists($this, $method))
                continue;
            
            $events[$event] = $method;
        }

        return $events;
    }

    public function patchEntity(EntityInterface $entity, array $data, array $options = [])
    {
        $this->dispatchEvent('Model.beforePatchEntity', compact('entity', 'data', 'options'));
        parent::patchEntity($entity, $data, $options);
        $this->dispatchEvent('Model.afterPatchEntity', compact('entity', 'options'));

        return $entity;
    }

    public function getEventfulEventMap()
    {
        return $this->_eventfulEventMap;
    }
}
