<?php
namespace Riddlemd\Tools\Model\Table;

use Cake\ORM\Query;
use Cake\Utility\Hash;
use Riddlemd\Tools\I18n\Time;

trait RequestQueryableTableTrait
{
    protected $_globalSearchFields = [];
    protected $_presentationFields = [];

    public function getGlobalSearchFields() : array
    {
        return $this->_globalSearchFields;
    }

    public function setGlobalSearchFields(array $fields) : RequestQueryableTableInterface
    {
        $this->_globalSearchFields = $fields;
        return $this;
    }

    public function getPresentationFields() : array
    {
        return $this->_presentationFields;
    }

    public function setPresentationFields(array $fields) : RequestQueryableTableInterface
    {
        $this->_presentationFields = $fields;
        return $this;
    }

    public function generateQueryFromKeyValuePairs(array $keyValuepairs, array $options = []) : Query
    {
        if(!is_a($this, '\Cake\ORM\Table'))
            throw new \Cake\Http\Exception\InternalErrorException(__METHOD__ . ' requires class inherit from Cake\ORM\Table');

        $primaryModel = $this;
        $primaryModelAlias = $primaryModel->getAlias();

        $options = array_merge([
            'associations' => true,
            'textWildcards' => true,
            'autoQueryKey' => 'q',
            'globalQueryKey' => 'gq',
            'matchAll' => true
        ], $options);

        $tableQuery = $primaryModel->find();

        // If query contains the autoQueryKey, add autoQueryKey's value to the query as models display field.
        if(isset($keyValuepairs[$options['autoQueryKey']]))
        {
            $keyValuepairs[$primaryModel->getDisplayField()] = $keyValuepairs[$options['autoQueryKey']];
            unset($keyValuepairs[$options['autoQueryKey']]);
        }

        // If query contains the globalQueryKey, adds globalSearchFields to keyValuepairs and set matchAll to false
        if(isset($keyValuepairs[$options['globalQueryKey']]) && $keyValuepairs[$options['globalQueryKey']] !== '')
        {
            foreach($this->getGlobalSearchFields() as $globalSearchField)
            {
                $keyValuepairs[$globalSearchField] = $keyValuepairs[$options['globalQueryKey']];
            }
            unset($keyValuepairs[$options['globalQueryKey']]);
            $options['matchAll'] = false;
        }

        $queryParts = [];

        foreach($keyValuepairs as $key => $value)
        {
            $model = $primaryModel;
            $modelAlias = $primaryModelAlias;
            
            if($options['associations'] && strpos($key, '.'))
            {
                $key = explode('.', $key);
                $modelAlias = $key[0];
                $key = $key[1];
                if(!isset($primaryModel->$modelAlias) || !is_a($primaryModel->$modelAlias, '\Cake\ORM\Association') || !is_a($primaryModel->$modelAlias->getTarget(), '\Riddlemd\Tools\Model\Table\keyValuepairsableTableInterface'))
                    continue;

                $model = $primaryModel->$modelAlias->getTarget();
            } 

            if($value !== '')
            {
                $useLike = strpos($value, '%') !== false || $options['textWildcards'];

                try
                {
                    $columnSchema = $model->getSchema()->getColumn($key);
                    if(!empty($columnSchema))
                    {
                        if($columnSchema['null'] && $value === 'NULL')
                        {
                            $queryParts[] = [
                                $modelAlias . '.' . $key . ' IS NULL',
                            ];
                        }
                        elseif($columnSchema['type'] == 'string')
                        {
                            $queryParts[] = [
                                $modelAlias . '.' . $key . ($useLike ? ' LIKE' : '') => $value . ($options['textWildcards'] ? '%' : '')
                            ];
                        }
                        elseif($columnSchema['type'] == 'datetime')
                        {
                            $timeObject = new Time($value);
                            $queryParts[] = [
                                $modelAlias . '.' . $key => $timeObject->I18nFormat(Time::I18N_SQL_DATETIME)
                            ];
                        }
                        elseif($columnSchema['type'] == 'date')
                        {
                            $timeObject = new Time($value);
                            $queryParts[] = [
                                $modelAlias . '.' . $key => $timeObject->I18nFormat(Time::I18N_SQL_DATE)
                            ];
                        }
                        elseif($columnSchema['type'] == 'json')
                        {
                            $queryParts[] = [
                                $modelAlias . '.' . $key . ($useLike ? ' LIKE' : '') => $value . ($options['textWildcards'] ? '%' : '')
                            ];
                        }
                        else
                        {
                            if($columnSchema['type'] == 'integer' && !is_numeric($value)) continue;

                            $queryParts[] = [
                                $modelAlias . '.' . $key => $value
                            ];
                        }
                    }
                }
                catch(\Exception $e)
                {
                    // Do nothing...
                }
            }
        }

        if(!empty($queryParts) && !$options['matchAll'])
        {
            $tableQuery->where(['OR' => $queryParts]);
        } else {
            $tableQuery->where($queryParts);
        }

        return $tableQuery;
    }
}