<?php
namespace Riddlemd\Tools\Model\Entity;

trait CachingEntityTrait
{
    protected $_cachingEntityTraitCache = [];

    protected function readFromCache(?string $key = null)
    {
        return $key === null ? $this->_cachingEntityTraitCache : ($this->_cachingEntityTraitCache[$key] ?? null);
    }

    protected function writeToCache(string $key, $value)
    {
        $this->_cachingEntityTraitCache[$key] = $value;
        return $this;
    }

    protected function isInCache(string $key) : bool
    {
        return isset($this->_cachingEntityTraitCache[$key]);
    }

    protected function clearFromCache(string $key)
    {
        unset($this->_cachingEntityTraitCache[$key]);
        return $this;
    }
}