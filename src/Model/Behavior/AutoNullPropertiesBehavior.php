<?php
namespace Riddlemd\Tools\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

class AutoNullPropertiesBehavior extends EventfulBehavior
{
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $schema = $this->_table->getSchema();
        
        foreach($entity->getDirty() as $dirtyPropertyName)
        {
            $propertySchema = $schema->getColumn($dirtyPropertyName);

            if(!empty($propertySchema) && $propertySchema['null'] && empty($entity->$dirtyPropertyName))
                $entity->$dirtyPropertyName = null;
        }
    }
}