<?php
namespace Riddlemd\Tools\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventManager;

use Cake\Utility\Inflector;

class ListAssociationReplaceBehavior extends EventfulBehavior
{
    protected $_defaultConfig = [
        'associations' => []
    ];

    public function beforePatchEntity(Event $event, EntityInterface $entity, array $data, array $options)
    {
        if(!isset($options['associated']) || !is_array($options['associated']) || !is_array($this->config('associations')))
            return;

        foreach($options['associated'] as $associatedModelName)
        {
            $tableName = Inflector::tableize($associatedModelName);
            $associationInformation = $this->_table->association($associatedModelName);
            $associationDisplayField = $associationInformation->target()->getDisplayField();
            $originalValues = $entity->getOriginal($tableName);

            if(!isset($originalValues) || !is_array($originalValues) || !isset($entity->$tableName) || !is_array($entity->$tableName))
                continue;

            foreach($entity->$tableName as $associatedEntity)
            {
                if($associatedEntity->id)
                    continue;

                foreach($originalValues as $originalAssociatedEntity)
                {
                    if($associatedEntity->$associationDisplayField === $originalAssociatedEntity->$associationDisplayField)
                        $associatedEntity->id = $originalAssociatedEntity->id;
                }
            }
        }
    }
}