<?php
namespace Riddlemd\Tools\Model\Behavior;

use Cake\ORM\Behavior as BaseBehavior;

class EventfulBehavior extends BaseBehavior
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        if(!is_a($this->_table, '\Riddlemd\Tools\Model\Table\EventfulTable'))
            throw new \Cake\Http\Exception\InternalErrorException(static::class . ' requires Table to inherit from Riddlemd\Tools\Model\Table\EventfulTable');
    }
    
    public function implementedEvents()
    {
        $events =  parent::implementedEvents();

        foreach ($this->_table->getEventfulEventMap() as $event => $method) {
            if (!method_exists($this, $method))
                continue;
            
            $events[$event] = $method;
        }
        
        return $events;
    }
}