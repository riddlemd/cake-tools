<?php
namespace Riddlemd\Tools\Utility;

abstract class Html
{
    public static function getInputValues(string $html)
    {
        $document = @\DOMDocument::loadHTML($html);

        $values = [];
        
        foreach($document->getElementsByTagName('select') as $element)
        {
            $name = $element->getAttribute('name');
            $value = '';

            foreach($element->childNodes as $childNode) {
                if(!$childNode->hasAttribute('selected'))
                    continue;

                $value = $childNode->hasAttribute('value') ? $childNode->getAttribute('value') : $childNode->textContent;
            }

            if(!empty($name))
                $values[$name] = $value;
        }

        foreach($document->getElementsByTagName('textarea') as $element)
        {
            $name = $element->getAttribute('name');
            $value = $element->textContent;

            if(!empty($name))
                $values[$name] = $value;
        }

        foreach($document->getElementsByTagName('input') as $element)
        {
            $name = $element->getAttribute('name');
            $value = $element->getAttribute('value');
            $type = $element->getAttribute('type');

            if($type == 'button' || $type == 'submit')
                continue;

            if(!empty($name))
                $values[$name] = $value;
        }

        return $values;
    }
}