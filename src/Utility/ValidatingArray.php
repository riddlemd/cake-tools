<?php
namespace Riddlemd\Tools\Utility;

class ValidatingArray extends \ArrayObject
{
    protected $_schema = [];

    protected $_validators = [];

    public function __construct($array = [])
    {
        $this->_validators = [
            null => function($v) { return true; },
            'string' => function($v, int $min = 0, ?int $max = null) { return is_string($v) && strlen($v) >= $min && ($max == null ? true : strlen($v) <= $max); },
            'int' => function($v, int $min = PHP_INT_MIN, int $max = PHP_INT_MAX) { return is_int($v) && $v >= $min && $v <= $max; },
            'float' => function($v, float $min = PHP_FLOAT_MIN, float $max = PHP_FLOAT_MAX) { return is_float($v) && $v >= $min && $v <= $max; },
            'bool' => function($v) { return is_bool($v); },
            'array' => function($v) { return is_array($v); },
            'object' => function($v) { return is_object($v); },
            'null' => function($v) { return is_null($v); },
            'resource' => function($v) { return is_resource($v); },
            'class' => function($v, ...$args) {return is_a($v, $args[0] ?? null); }
        ];

        parent::__construct($array);
    }

    public function offsetSet($offset, $value) : void
    {
        $value = $this->_isValid($offset, $value) ? $value : $this->_schema[$offset]['default'] ?? null;
        parent::offsetSet($offset, $value);
    }

    public function setSchema($key, $default = null, $validator = null, ...$validatorArgs) : OptionsArray
    {
        $this->_schema[$key] = [];
        $this->_schema[$key]['validator'] = $validator;
        $this->_schema[$key]['validatorArgs'] = $validatorArgs;
        $this->_schema[$key]['default'] = $this->_isValid($key, $default) ? $default : null;

        $this[$key] = $this[$key] ?? $default;

        return $this;
    }

    protected function _isValid($key, $value) : bool
    {
        if(!isset($this->_schema[$key]))
            return true;

        $args = $this->_schema[$key]['validatorArgs'];
        array_unshift($args, $value);

        if(is_callable($this->_schema[$key]['validator']))
            return $this->_schema[$key]['validator'](...$args);

        if(isset($this->_validators[$this->_schema[$key]['validator']]))
            return $this->_validators[$this->_schema[$key]['validator']](...$args);

        return false;
    }
}