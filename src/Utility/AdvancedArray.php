<?php
namespace Riddlemd\Tools\Utility;

use Cake\Utility\Hash;

class AdvancedArray extends \ArrayObject
{
    public function write($key, $value = null) : SmartArray
    {
        $this->exchangeArray(Hash::insert($this, $key, $value));  
        return $this;
    }

    public function read($keyPath) : String
    {
        if($keyPath === null) return null;
        return Hash::get($this, $keyPath);
    }

    public function check($key) : bool
    {
        return $this->read($key) !== null;
    }

    public function delete($key) : SmartArray
    {
        $this->exchangeArray(Hash::remove($this, $key));
        return $this;
    }

    public function getFirst()
    {
        $result = reset($this);
        return $result ? $result : null;
    }

    public function getLast()
    {
        $result = end($this);
        return $result ? $result : null;
    }

    public function getNext()
    {
        $result = next($this);
        return $result ? $result : null;
    }

    public function getPrev()
    {
        $result = prev($this);
        return $result ? $result : null;
    }

    public function isAssociative()
    {
        $count = count($this);
        return $count ? array_keys($this) !== range(0, $count - 1) : true;
    }
}