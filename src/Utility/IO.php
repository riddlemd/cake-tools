<?php
namespace Riddlemd\Tools\Utility;

abstract class IO
{
    public static function rglob($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags); 
        foreach (glob(dirname($pattern) . DS . '*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, IO::rglob($dir. DS .basename($pattern), $flags));
        }
        return $files;
    }

    public static function sizeStringToBytes($size)
    {
        $units = 'bkmgtpezy';
        $matches = [];
        if(!preg_match("/([\d]+)([{$units}])/i", $size, $matches))
            throw new \Exception('Invalid Size String');
        $unit = strtolower($matches[2]);
        $size = $matches[1];
        $exponent = strpos($units, $unit[0]);
        return \round($size * pow(1024, $exponent));
    }
}