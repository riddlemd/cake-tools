<?php
namespace Riddlemd\Tools\Utility;

abstract class Text
{
    public function isCyrillic($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }
}