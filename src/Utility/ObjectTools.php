<?php
namespace Riddlemd\Tools\Utility;

abstract class ObjectTools
{
    public function cast(string $newClassName, object $objectToCast) {
        $serializedObjectString = serialize($objectToCast);
        $regexpString = '/^O:(\d+):"([\w\\\]+)":/';
        $replaceString = sprintf('O:%d:"%s":', strlen($newClassName), $newClassName);
        $serializedObjectString = preg_replace($regexpString, $replaceString, $serializedObjectString);
        $newObject = unserialize($serializedObjectString);
        if(get_class($newObject) === '__PHP_Incomplete_Class') throw new \Exception(sprintf('%s does not inherit from %s', get_class($objectToCast), $newClassName));
        return $newObject;
    }
}