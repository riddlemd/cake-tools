<?php
namespace Riddlemd\Tools\Utility;

class Inflector extends \Cake\Utility\Inflector
{
    public static function possessive(string $string) : string
    {
        return $string . '\'' . (substr($string, -1, 1) != 's' ? 's' : '');
    }

    public static function pascalCaseToHumanized(string $string) : string
    {
        return implode(' ', preg_split('/([A-Z]+[^A-Z]*)/', $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE));
    }
}