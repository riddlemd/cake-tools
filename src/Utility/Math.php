<?php
namespace Riddlemd\Tools\Utility;

abstract class Math
{
    public static function normalizeRange($value, $low, $high)
    {
        return ($value - $low) / ($high - $low);
    }

    public static function inRange($value, $min, $max)
    {
        $result = filter_var(
            $value,
            FILTER_VALIDATE_INT,
            [
                'options' => [
                    'min_range' => $min,
                    'max_range' => $max
                ]
            ]
        );
        return $result ? true : false;
    }
}