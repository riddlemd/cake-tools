<?php
namespace Riddlemd\Tools\Utility;

abstract class Cryptography
{
    public static function generateUID(int $length = 16) : string
    {
        return bin2hex(random_bytes($length));
    }
}