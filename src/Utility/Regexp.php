<?php
namespace Riddlemd\Tools\Utility;

abstract class Regexp
{
    public function match(string $pattern, string $subject)
    {
        $matches = [];
        \preg_match($pattern, $subject, $matches);
        array_shift($matches);
        return $matches;
    }
}