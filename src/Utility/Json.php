<?php
namespace Riddlemd\Tools\Utility;

abstract class Json
{
    public static function encode($input, int $options = 0, int $depth = 512) : string
    {
        $json = json_encode($input, $options);
        if(json_last_error() !== \JSON_ERROR_NONE) throw new JsonException;
        return $json;
    }

    public static function decode(string $input, bool $assoc = false, int $depth = 512, int $options = 0)
    {
        $json = json_decode($input, $assoc, $depth, $options);
        if(json_last_error() !== \JSON_ERROR_NONE) throw new JsonException('JSON Invalid');
        return $json;
    }
}

class JsonException extends \Exception
{

}