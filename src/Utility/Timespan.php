<?php
namespace Riddlemd\Tools\Utility;

use Cake\I18n\FrozenTime;

class Timespan
{
    protected $_start;
    protected $_end;
    protected $_length;

    public function __construct($start, $end)
    {
        $start = new FrozenTime($start);
        $end = new FrozenTime($end);
        $this->_start = $start;
        $this->_end = $end;
        $this->_length = $end->getTimestamp() - $start->getTimestamp();
    }

    public function getStart()
    {
        return $this->_start;
    }

    public function getEnd()
    {
        return $this->_end;
    }

    public function getLength()
    {
        return $this->_length;
    }

    public function contains($time)
    {
        $time = FrozenTime($time);
        return $this->_start < $time && $time < $this->_end;
    }

    public function overlaps(Timespan $timespan)
    {
        return $this->_start <= $timespan->getEnd() && $timespan->getStart() <= $this->_end;
    }

    public function diff(Timespan $timespan)
    {
        if(!$this->overlaps($timespan))
            return $this;

        $results = [];

        if($timepsan->compare($this) <= 0)
        {
            if($this->_end > $timespan->getEnd())
                $results[] = new $this($timespan->getEnd(), $this->_end);
        }
        else
        {
            if($timespan->getEnd() < $this->_end)
            {
                $results[] = new $this($this->_start, $timespan->getStart());
                $results[] = new $this($timespan->getEnd(), $this->_end);
            }
            else
            {
                $results = $this($this->_start, $timespan->getStart());
            }
        }

        return $results;
    }

    public function trim($start, $end)
    {
        $start = new FrozenTime($start);
        $end = new FrozenTime($end);

        if($start >= $this->_end || $end <= $this->_start)
            return null;

        if($start < $this->_start)
            $start = $this->_start;

        if($end > $this->_end)
            $end = $this->_end;

        if($start == $end)
            return null;

        return new $this($start, $end);
    }

    public function compare(Timespan $timespan)
    {
        if($this->_start == $timespan->getStart())
            return 0;

        return $this->_start < $timespan->getStart() ? -1 : 1;
    }

    public function toArray()
    {
        return [
            'start' => $this->_start->format('c'),
            'end' => $this->_end->format('c')
        ];
    }

    public function toString()
    {
        return $this->_start->format('c') . '/' . $this->_end->format('c');
    }
}