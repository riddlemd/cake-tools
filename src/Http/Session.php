<?php
namespace Riddlemd\Tools\Http;

use Cake\Http\Session as CakeSession;
use Cake\ORM\TableRegistry;

class Session extends CakeSession
{
    public static function deleteById(string $sessionId, string $mode = 'php')
    {
        if($mode == 'database')
        {
            $sessionsTable = TableRegistry::get('Sessions');
            $session = $this->SessionsTable->findById($sessionId);

            if(!$session) return;

            $sessionTable->delete($session);
        }
        else if($mode == 'php')
        {
            $currentSessionId = session_id();
            session_write_close();

            session_id($sessionId);
            session_start();
            session_destroy();
            session_write_close();

            if($currentSessionId)
            {
                session_id($currentSessionId);
                session_start();
            }
        }
        else throw new \Exception('Invalid mode');
    }
}