(function(window) {
    "use strict";
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.Timespan = function Timespan(start, end) {
        if(!(start instanceof Date)) throw 'Argument #1 must be Date Object';
        if(!(end instanceof Date)) throw 'Argument #2 must be Date Object';
        var length = (end.getTime() - start.getTime()) / 1000;

        this.getStart = function() {
            return start;
        };

        this.getEnd = function() {
            return end;
        };

        this.getLength = function() {
            return length;
        };

        this.contains = function(time) {
            if(!(time instanceof Date)) throw 'Argument #1 must be Date Object';
            return start <= time && time <= end;
        };

        this.overlaps = function(timespan) {
            if(!(timespan instanceof Timespan)) throw 'Argument #1 must be Timespan Object';
            return start <= timespan.getEnd() && timespan.getStart() <= end;
        };

        this.compare = function(timespan) {
            if(!(timespan instanceof Timespan)) throw 'Argument #1 must be Timespan Object';
            if(start == timespan.getStart()) return 0;
            return start < timespan.getStart() ? -1 : 1;
        }
    };
})(window);