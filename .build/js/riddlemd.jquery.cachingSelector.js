// Caching jQuery selector
(function(window, $) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};
    var cache = {};
    
    window.Riddlemd.cachingSelector = function(query, context, forceUpdate) {
        if(typeof cache[context] == 'undefined')
            cache[context] = {};

        if(forceUpdate || typeof cache[context][query] == 'undefined')
            cache[context][query] = $(query, context);

        return cache[context][query];
    }
}(window, jQuery));