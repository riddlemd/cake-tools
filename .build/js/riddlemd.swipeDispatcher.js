// Swipe Dispatcher
(function(window) {
    var previousTouchEvents = {};
    var previousClickEvents = {};

    function touchStartHandler(event) {
        previousTouchEvents[this] = event;
    };

    function touchEndHandler(event) {
        if(!previousTouchEvents[this])
            return;

        var diffX = previousTouchEvents[this].touches[0].clientX - event.changedTouches[0].clientX;
        var diffY = previousTouchEvents[this].touches[0].clientY - event.changedTouches[0].clientY;

        var swipe = {};
        swipe.up = diffY > 0
        swipe.right = diffX < 0;
        swipe.down = diffY < 0;
        swipe.left = diffX > 0;
        swipe.strengthX = diffX;
        swipe.strengthY = diffY;

        var swipeEvent = new CustomEvent('swipe', {detail:swipe, bubbles: true});
        event.path[0].dispatchEvent(swipeEvent);

        previousTouchEvents[this] = null;
    };

    function mouseDownHandler(event) {
        previousClickEvents[this] = event;
    };

    function mouseUpHandler(event) {
        if(!previousClickEvents[this])
            return;

        var diffX = previousClickEvents[this].clientX - event.clientX;
        var diffY = previousClickEvents[this].clientY - event.clientY;

        var swipe = {};
        swipe.up = diffY > 0
        swipe.right = diffX < 0;
        swipe.down = diffY < 0;
        swipe.left = diffX > 0;
        swipe.strengthX = diffX;
        swipe.strengthY = diffY;

        var swipeEvent = new CustomEvent('swipe', {detail:swipe, bubbles: true});
        previousClickEvents[this].path[0].dispatchEvent(swipeEvent);

        previousClickEvents[this] = null;
    };

    window.document.addEventListener('touchstart', touchStartHandler, false);
    window.document.addEventListener('touchend', touchEndHandler, false);

    window.document.addEventListener('mousedown', mouseDownHandler, false);
    window.document.addEventListener('mouseup', mouseUpHandler, false);
}(window));