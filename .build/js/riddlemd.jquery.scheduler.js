(function(window) {
    $.widget('djc.scheduler', {
        options: {
            schedule: [],
            events: [],
            defaultEventLength: null,

            // events
            afterDateSelected: null,
            afterTimeSelected: null,

            beforeTimesRendered: null,
            afterTimesRendered: null,

            afterTimeRendered: null,
        },

        datesElement: null,
        timesElement: null,
        
        _create: function() {
            var currentWidget = this;

            if(!moment.isDuration(currentWidget.options.defaultEventLength)) currentWidget.options.defaultEventLength = moment.duration(currentWidget.options.defaultEventLength);
            if(currentWidget.options.defaultEventLength.asSeconds() <= 0) currentWidget.options.defaultEventLength = moment.duration(15, 'minutes');

            currentWidget.element
                .addClass('scheduler');

            currentWidget.datesElement = $('<ul>')
                .addClass('dates')
                .appendTo(currentWidget.element);

            currentWidget.timesElement = $('<ul>')
                .addClass('times')
                .appendTo(currentWidget.element);

            currentWidget._renderDates();
        },
        _renderDates: function() {
            var currentWidget = this;
            $.each(currentWidget.options.schedule, function(key, timeFrame) {
                var start = moment(timeFrame.start);
                var end = moment(timeFrame.end);
                var fullDate = start.format('YYYY-MM-DD');
                var day = start.format('ddd');
                var month = start.format('MMM');
                var date = start.format('DD');

                var $dateElement = $('<li>')
                    .addClass('date')
                    .attr({
                        'data-start': start.toISOString(),
                        'data-end': end.toISOString(),
                        'data-day': day,
                    })
                    .html('<div class=\'day\'>' + day + '</div><div class=\'date\'>' + month + ' ' + date + '</div>')
                    .click(function(e) {
                        var $currentListItem = $(this);

                        var start = moment($currentListItem.data('start'));
                        var end = moment($currentListItem.data('end'));

                        if(!$currentListItem.hasClass('disabled')) {
                            $currentListItem
                                .parent()
                                    .children()
                                        .removeClass('selected')
                                        .end()
                                    .end()
                                .addClass('selected');
                        }

                        currentWidget._trigger('afterDateSelected', e, {
                            start: start,
                            end: end
                        });

                        currentWidget._renderTimes(start, end);
                    })
                    .appendTo(currentWidget.datesElement);
            });
        },
        _renderTimes: function(start, end) {
            var currentWidget = this;

            function render(events) {
                for(var i = start.unix(); i < end.unix(); i = i + currentWidget.options.defaultEventLength.asSeconds()) {

                    var timeSegmentStart = moment.unix(i);
                    var timeSegmentEnd = timeSegmentStart.clone().add(currentWidget.options.defaultEventLength);
                    var timeEvents = [];
                    $.each(events, function(key, event) {
                        var eventStart = moment(event.start);
                        var eventEnd = moment(event.end);
                        if((eventStart < timeSegmentEnd && timeSegmentStart < eventEnd) || (eventStart == timeSegmentStart && eventEnd == timeSegmentEnd)) {
                            timeEvents.push(event);
                        }
                    });
                    var $timeElement = $('<li>')
                        .addClass('time')
                        .attr({
                            'data-start': timeSegmentStart.toISOString(),
                            'data-end': timeSegmentEnd.toISOString()
                        })
                        .data('events', timeEvents)
                        .html(timeSegmentStart.format('hh:mma'))
                        .click(function(e) {
                            var $currentListItem = $(this);

                            var start = moment($currentListItem.data('start'));
                            var end = moment($currentListItem.data('end'));

                            if(!$currentListItem.hasClass('disabled')) {
                                $currentListItem
                                    .parent()
                                        .children()
                                            .removeClass('selected')
                                            .end()
                                        .end()
                                    .addClass('selected');

                                currentWidget._trigger('afterTimeSelected', e, {
                                    start: start,
                                    end: end
                                });
                            }
                        })
                        .appendTo(currentWidget.timesElement);

                    currentWidget._trigger('afterTimeRendered', null, {
                        element: $timeElement
                    });
                }
            };

            currentWidget.timesElement.empty();

            currentWidget._trigger('beforeTimesRendered');

            if(typeof currentWidget.options.events == 'function') {
                currentWidget.options.events.apply(currentWidget, [{start:start,end:end}, render]);
            } else {
                render(currentWidget.options.events);
            }

            currentWidget._trigger('afterTimesRendered');
        },
        getSelectedDate: function() {
            return moment(this.datesElement.find('.selected').data('startDate'));
        },
        getSelectedTimeFrame: function() {
            return {
                start: moment(this.timesElement.find('.selected').data('startDate')),
                end: moment(this.timesElement.find('.selected').data('endDate')),
            }
        },
        reset: function() {
            var currentWidget = this;

            currentWidget.datesElement.empty();
            currentWidget.timesElement.empty();

            currentWidget._renderDates();
        }
    });
}(window));