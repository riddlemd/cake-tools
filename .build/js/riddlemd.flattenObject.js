(function($, window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.flattenObject = function(object, prefix, root, separator) {
        root = root || {};
        prefix = prefix || '';
        separator = separator || '.';
        for(var key in object) {
            var value = object[key];
            if(typeof value === 'object') {
                root = window.Riddlemd.flattenObject(value, prefix + key + separator, root, separator);
            } else {
                key = prefix + key;
                root[key] = value;
            }
        }
        return root;
    };
}(jQuery, window));