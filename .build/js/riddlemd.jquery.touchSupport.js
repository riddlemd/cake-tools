// Add touch support to jQuery UI Slider
(function(window) {
    var $targetSliderHandle;

    $(window.document)
        .on('touchstart', '.ui-slider > .ui-slider-handle', function(e) {
            $targetSliderHandle = $(this);
        })
        .on('touchend', '.ui-slider > .ui-slider-handle', function(e) {
            $targetSliderHandle = null;
        })
        .on('touchmove', '.ui-slider', function(e) {
            var $currentSlider = $(this);

            if($targetSliderHandle) {
                var isRange = $currentSlider.slider('option', 'range');

                var offset = $currentSlider.offset();
                var width = $currentSlider.innerWidth();
                var max = $currentSlider.slider('option', 'max');
                var values = $currentSlider.slider('values');
                var percentage = (e.changedTouches[0].clientX - offset.left) / width;
                var newValue = percentage * max;

                if(isRange) {
                    var index = $targetSliderHandle.index() - 1;
                    if(index === 0 && newValue > values[1]) {
                    newValue = values[1];
                    }
                    else if(index === 1 && newValue < values[0]) {
                        newValue = values[0];
                    }

                    $currentSlider.slider('values', index, newValue);
                } else {
                    $currentSlider.slider('value', newValue);
                }
            }
        });
}(window));