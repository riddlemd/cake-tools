(function(window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.defaultTo = function(value, defaultValue, options) {
        if(typeof defaultValue === 'undefined') throw 'defaultValue required!';
        if(typeof value === 'undefined' || value === null) return defaultValue;
        return value;
    }
}(window));