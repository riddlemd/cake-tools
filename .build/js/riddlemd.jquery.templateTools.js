(function($, window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};
    if(typeof $ !== 'function') throw 'jQuery required!';

    window.Riddlemd.TemplateTools = {};

    window.Riddlemd.TemplateTools.getTemplate = function(template, tokens, tokenPattern) {
        tokens = typeof tokens === 'object' ? tokens : {};
        var html = $('template#' + template).first().html() || '';
        var tokensInTemplate = window.Riddlemd.TemplateTools.getTokensInString(html, tokenPattern);
        tokens = window.Riddlemd.flattenObject(tokens);
        $.each(tokensInTemplate, function() {
            if(typeof tokens[this] === 'undefined') {
                tokens[this] = '';
            }
        });
        html = window.Riddlemd.TemplateTools.replaceTokensInString(html, tokens);
        return html;
    };

    window.Riddlemd.TemplateTools.getTokensInString = function(string, tokenPattern) {
        var matches = [];
        var tokens = [];
        var regexp = tokenPattern instanceof RegExp ? tokenPattern : /{([^}]+)}/g;
        while(matches = regexp.exec(string)) {
            tokens.push(matches[1]);
        }
        return tokens;
    };

    window.Riddlemd.TemplateTools.regExpEscape = function(string) {
        return string.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };

    window.Riddlemd.TemplateTools.replaceTokensInString = function(string, tokens, tokenPattern) {
        tokens = typeof tokens === 'object' ? tokens : {};
        tokenPattern = typeof tokenPattern === 'string' ? tokenPattern : '{{token}}';
        string = string.replace(new RegExp('{_tokens_}', 'g'), JSON.stringify(tokens));
        $.each(tokens, function(token, replacement) {
            token = window.Riddlemd.TemplateTools.regExpEscape(tokenPattern.replace('{token}', token));
            replacement = replacement || '';
            string = string.replace(new RegExp(token, 'g'), replacement);
        });
        return string;
    };
}(jQuery, window));