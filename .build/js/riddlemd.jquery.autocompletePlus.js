(function($, window) {
    $.widget('Riddlemd.autocompletePlus', $.ui.autocomplete, {
        options : {
            masked : false,
            requireFromSource : false,
            attributesToIgnore : [
                'id',
                'class',
                'maxlength',
                'data-mask-field-value'
            ],
            // Events
            afterChange : null
        },
        _create : function() {
            var currentWidget = this;
            var callbacks = {};
            callbacks.change = currentWidget.options.change;
            callbacks.close = currentWidget.options.close;
            callbacks.create = currentWidget.options.create;
            callbacks.focus = currentWidget.options.focus;
            callbacks.open = currentWidget.options.open;
            callbacks.response = currentWidget.options.response;
            callbacks.search = currentWidget.options.search;
            callbacks.select = currentWidget.options.select;

            if(currentWidget.options.masked) {
                currentWidget.element.parent().addClass('autocomplete-plus-parent');

                currentWidget.originalElement = currentWidget.element
                    .addClass('autocomplete-plus-master')
                    .attr({type : 'hidden'});

                let attrs = {};
                $.each(currentWidget.originalElement[0].attributes, function() {
                    if(this.specified && currentWidget.options.attributesToIgnore.indexOf(this.name) === -1) {
                        attrs[this.name] = this.value;
                    }
                });

                attrs.type = 'text';
                attrs.name = '';
                attrs.autocomplete = 'off';

                currentWidget.element = $('<input>')
                    .attr(attrs)
                    .addClass('autocomplete-plus-slave')
                    .insertAfter(currentWidget.originalElement)
                    .change(function() {
                        var $currentField = $(this);
                        if(!$currentField.val().length) {
                            currentWidget.originalElement.val('');
                        }
                    });

                if(currentWidget.originalElement.val().length) {
                    if(currentWidget.originalElement.data('maskFieldValue')) {
                        currentWidget.element.val(currentWidget.originalElement.data('maskFieldValue'));
                    }
                }

                if(currentWidget.originalElement.data('maskFieldName')) {
                    currentWidget.element.attr('name', currentWidget.originalElement.data('maskFieldName'));
                }
            }

            currentWidget.options.select = function(event, ui) {
                if(typeof callbacks.select == 'function') {
                    callbacks.select.apply(this, [event, ui]);
                }

                if(currentWidget.options.masked) {
                    if(!event.isDefaultPrevented()) {
                        event.preventDefault();
                        currentWidget.element.val(ui.item.label);
                        currentWidget.originalElement.val(ui.item.value);
                    }
                }
            };

            currentWidget.options.change = function(event, ui) {
                if(typeof callbacks.change == 'function') {
                    callbacks.change.apply(this, [event, ui]);
                }

                if(!event.isDefaultPrevented()) {
                    event.preventDefault();
                    if(currentWidget.options.requireFromSource) {
                        if(!ui.item) {
                            currentWidget.element
                                .val('')
                                .change();
                        }
                    }

                    if(typeof currentWidget.options.afterChange == 'function') {
                        currentWidget.options.afterChange.apply(this, [event, ui]);
                    }
                }

            }

            currentWidget._super();
        },
        getElement : function() {
            var currentWidget = this;
            return currentWidget.element;
        }
    });
}(jQuery, window));