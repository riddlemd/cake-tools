(function(scope) {
    if(typeof scope.Riddlemd !== 'object') scope.Riddlemd = {};

    function loadGoogleMap(key, callback) {
        if(typeof callback === 'function') {
            var _callback = callback;
            callback = 'gmapCallback' + (new Date()).getTime();

            scope[callback] = _callback;
        }
        
        var scriptElement = document.createElement('script');
        scriptElement.setAttribute('async', true);
        scriptElement.setAttribute('defer', true);
        scriptElement.src = 'https://maps.googleapis.com/maps/api/js?key=' + key + '&callback=' + callback;
        scope.document.body.appendChild(scriptElement);
    };
    scope.Riddlemd.loadGoogleMap = loadGoogleMap;
})(this);