(function($, window) {
    var $document = $(window.document);

    $.widget('Riddlemd.smartElement', {
        options: {
            autoSort: false,
            fieldName: null,
            items: [],
            primaryKey: null,
            autoFillFormElements: true,
            sortable: null,
            addPrepends: false,
            getItemDataHandler: null,
            isItemElementValidHandler: null,
            weightFieldName: null,
            autoAddEmptyLastItem: false,
            enumerated: true,
            allowEmpty: false,
            // Specifiers
            removeLinkSpecifiers: '.remove-link',
            // Callbacks
            beforeRemove: null,
            beforeItemAdded: null,
            itemAdded: null,
            beforeSubmit: null,
            submit: null
        },
        _create: function() {
            var currentWidget = this;

            currentWidget.element.addClass('smart-element');

            currentWidget.childContainer = currentWidget.element.prop('tagName') == 'TABLE' ? $(currentWidget.element.children('tbody')) : currentWidget.element;

            $document
                .on('submit', 'form', function(e) {
                    var $currentForm = $(this);

                    if($currentForm[0] == currentWidget._getForm()[0]) {
                        currentWidget._prepareForSubmit();
                        currentWidget._trigger('submit', e, {});
                    }
                })
                .on('click', 'input[type="submit"], button[type="submit"]', function(e) {
                    var $currentSubmit = $(this);
                    var $closestForm = $currentSubmit.closest('form');

                    if($closestForm[0] == currentWidget._getForm()[0] && currentWidget.options.autoAddEmptyLastItem) {
                        if(!currentWidget._trigger('beforeSubmit', null, {}))
                            e.preventDefault();

                        var $lastElement = currentWidget.getItemElements().last();
                        if(!currentWidget.isItemElementValid($lastElement)) {
                            $lastElement.find(':input')
                                .attr('name', '')
                                .prop('required', false);
                        }
                    }
                })

            currentWidget.element
                .on('change', ':input', function(e) {
                    var $currentInput = $(this);
                    
                    if($currentInput.data('smartElementManager') == currentWidget) {
                        if(currentWidget.options.autoAddEmptyLastItem) {
                            var $lastItemElement = currentWidget.getItemElements().last();
                            if(currentWidget.isItemElementValid($lastItemElement)) {
                                currentWidget.addItem({});
                            }
                        }
                    }
                })
                .on('keydown', 'input', function(e) {
                    if(e.keyCode == 13) e.preventDefault();
                })
                .on('click', currentWidget.options.removeLinkSpecifiers, function(e) {
                    e.preventDefault();

                    var elmTypeToLookFor = currentWidget.element.prop('tagName') == 'TABLE' ? 'tr' : 'li';
                    var $currentElm = $(this);
                    var $elmToRemove = $currentElm.closest(elmTypeToLookFor);

                    currentWidget.remove($elmToRemove);
                });

            if(currentWidget.options.sortable) {
                var sortableOptions =  {
                    items: currentWidget.options.allowEmpty ? '> *' : '> :not(:last-child)',
                    containment: currentWidget.childContainer.parent()
                };
                
                if(typeof currentWidget.options.sortable == 'object') {
                    $.extend(sortableOptions, currentWidget.options.sortable);
                }

                currentWidget.childContainer.sortable(sortableOptions);
            }

            if(currentWidget.options.items instanceof HTMLElement) currentWidget.options.items = $(currentWidget.options.value);

            try {
                if(currentWidget.options.items instanceof $) {
                    let inputElement = currentWidget.options.items;
                    currentWidget.options.items = JSON.parse(inputElement.val());
                    inputElement.val('null');
                }
            } catch {
                currentWidget.options.items = [];
            }

            $.each(currentWidget.options.items, function(itemKey, item) {
                currentWidget.addItem(item, itemKey);
            });

            if(currentWidget.options.autoAddEmptyLastItem) {
                currentWidget.addItem({});
            }
        },
        _addClearInput: function() {
            var currentWidget = this;
            if(currentWidget.options.fieldName) {
                var clearInput = $('<input>')
                    .attr({
                        name : currentWidget.options.fieldName,
                        type : 'hidden'
                    })
                    .prependTo(currentWidget._getForm());
            }
        },
        _prepareForSubmit: function() {
            var currentWidget = this;
            
            currentWidget._addClearInput();
            
            var itemElements = currentWidget.getItemElements();
            
            $.each(itemElements, function() {
                var $currentItemElement = $(this);
                var itemId = $currentItemElement.index();
                var $inputs = $currentItemElement.find(':input');

                if(currentWidget.options.weightFieldName) {
                    $inputs.filter('[name=' + currentWidget.options.weightFieldName + ']').val(itemId);
                }

                if(currentWidget.options.fieldName) {
                    $inputs.each(function() {
                        var $currentInput = $(this);

                        if($currentInput.attr('name')) {
                            if($currentInput.data('smartElementManager') == currentWidget) {
                                var fullName = currentWidget.fieldName();

                                var $parentSmartElementItem = currentWidget.element.closest('.smart-element-item');
                                if($parentSmartElementItem.length) {
                                    var $parentSmartElement = $parentSmartElementItem.closest('.smart-element');
                                    var parentSmartElementFieldName = $parentSmartElement.smartElement('fieldName');
                                    if(parentSmartElementFieldName) {
                                        fullName = parentSmartElementFieldName + '[' + $parentSmartElementItem.index() + '][' + currentWidget.fieldName() + ']';
                                    }
                                }

                                if(currentWidget.options.enumerated) {
                                    fullName += '[' + itemId + ']';
                                }
                                
                                fullName += '[' + $currentInput.attr('name') + ']';

                                $currentInput.attr('name', fullName);
                            }
                        }
                    });
                }
            });
        },
        _defaultGetItemDataHandler: function($item) {
            var currentWidget = this;
            var data = {};
            $item.find(':input').each(function() {
                var $currentInput = $(this);
                var name = $currentInput.attr('name');
                if(name) {
                    data[name] = $currentInput.val();
                }
            });
            return data;
        },
        _defaultIsItemElementValidHandler: function($item) {
            var currentWidget = this;
            var isValid = true;
            $item.find(':input[required]').each(function() {
                var $currentInput = $(this);
                if(!$currentInput.val()) {
                    isValid = false;
                }
            });
            return isValid;
        },
        _getForm: function() {
            var currentWidget = this;
            return currentWidget.element.closest('form');
        },
        fieldName: function(value) {
            var currentWidget = this;
            if(typeof value === 'string') {
                currentWidget.options.fieldName = value;
                return currentWidget.element;
            }
            return currentWidget.options.fieldName;
        },
        getItemData: function($item) {
            var currentWidget = this;
            if(typeof currentWidget.options.getItemDataHandler == 'function') {
                return currentWidget.options.getItemDataHandler($item);
            }
            return currentWidget._defaultGetItemDataHandler($item);
        },
        addItem: function(data, key) {
            var currentWidget = this;
            var $item = null;

            currentWidget.childContainer.find('.empty').remove();

            if(typeof data !== 'object') {
                data = {
                    value: data
                };
            }

            if(currentWidget.options.primaryKey) {
                currentWidget.getItemElements().each(function() {
                    var $currentItem = $(this);
                    var itemData = currentWidget.getItemData($currentItem);
                    if(itemData) {
                        if(itemData[currentWidget.options.primaryKey] == data[currentWidget.options.primaryKey]) {
                            $item = $currentItem;
                            return false;
                        }
                    }
                });
            }
  
            if(!$item) {
                $item = currentWidget.element.prop('tagName') == 'TABLE' ? $('<tr>') : $('<li>');

                $item.addClass('smart-element-item');

                if(currentWidget._trigger('beforeItemAdded', null, { item: $item, data: data, key: key})) {

                    if(currentWidget.options.addPrepends) {
                        $item.prependTo(currentWidget.childContainer);
                    } else {
                        $item.appendTo(currentWidget.childContainer);
                    }
                }
            }

            $item.data('smartElementData', data);

            if(currentWidget.options.autoFillFormElements) {
                var $inputs = $item.find(':input')
                    .each(function() {
                        var $currentInput = $(this);

                        if(!$currentInput.data('smartElementManager')) {
                            $currentInput.data('smartElementManager', currentWidget);
                        }

                        if($currentInput.data('smartElementManager') == currentWidget) {
                            var name = $currentInput.attr('name');
                            if(typeof data[name] !== 'undefined') {
                                var value = data[name];
                                if($currentInput.data('smartElementManager') == currentWidget) {
                                    if($currentInput.attr('type') == 'checkbox') {
                                        $currentInput.prop('checked', value ? true : false);
                                    } else {
                                        $currentInput.val(value);
                                    }
                                }
                            }
                        }
                    });

                if(typeof data.value !== 'undefined') {
                    $inputs.filter('[name=""]').val(data.value);
                }
            }

            currentWidget._trigger('itemAdded', null, {
                item: $item,
                data: data,
                key: key
            });
        },
        getItemElements: function() {
            var currentWidget = this;
            return currentWidget.childContainer.children();
        },
        isItemElementValid: function($element) {
            var currentWidget = this;
            $element = ($element instanceof $) ? $element : $($element);

            if(typeof currentWidget.options.isItemElementValidHandler == 'function') {
                return currentWidget.options.isItemElementValidHandler($element);
            }
            return currentWidget._defaultIsItemElementValidHandler($element);
            
        },
        remove: function(elm) {
            elm = elm instanceof $ ? elm : $(elm);
            var currentWidget = this;
            var eventResult = currentWidget._trigger('beforeRemove', null, {elm:elm});

            if(eventResult) {
                if(!currentWidget.options.allowEmpty && elm.is(':last-child')) return;

                elm.remove();
            }
        }
    });
}(jQuery, window));