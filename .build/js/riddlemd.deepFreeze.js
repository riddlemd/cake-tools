(function(scope) {
    function deepFreeze(obj) {
        if(typeof obj !== 'object') return;

        Object.freeze(obj);

        Object.getOwnPropertyNames(obj).forEach(function(prop) {
            if(typeof obj[prop] !== 'object') return;
            if(Object.isFrozen(obj[prop])) return;

            deepFreeze(obj[prop]);
        });
    };
    scope.deepFreeze = deepFreeze;
}(window));