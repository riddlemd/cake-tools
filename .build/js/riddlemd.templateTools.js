(function(window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.TemplateTools = {};

    function getTemplate(selector, tokens, tokenPattern) {
        tokens = typeof tokens === 'object' ? tokens : {};
        var element = window.document.querySelector(selector);
        var html = element ? element.innerHTML : '';
        var tokensInTemplate = getTokensInString(html, tokenPattern);
        tokens = window.Riddlemd.flattenObject(tokens);
        for(var key in tokensInTemplate) {
            var token = tokensInTemplate[key];
            if(typeof tokens[token] === 'undefined') {
                tokens[token] = '';
            }
        }
        html = replaceTokensInString(html, tokens);
        return html;
    };
    window.Riddlemd.TemplateTools.getTemplate = getTemplate;

    function getTokensInString(string, tokenPattern) {
        var matches = [];
        var tokens = [];
        var regexp = tokenPattern instanceof RegExp ? tokenPattern : /{([^}]+)}/g;
        while(matches = regexp.exec(string)) {
            tokens.push(matches[1]);
        }
        return tokens;
    };
    window.Riddlemd.TemplateTools.getTokensInString = getTokensInString;

    function regExpEscape(string) {
        return string.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };
    window.Riddlemd.TemplateTools.regExpEscape = regExpEscape;

    function replaceTokensInString(string, tokens, tokenPattern) {
        tokens = typeof tokens === 'object' ? tokens : {};
        tokenPattern = typeof tokenPattern === 'string' ? tokenPattern : '{{token}}';
        string = string.replace(new RegExp('{_tokens_}', 'g'), JSON.stringify(tokens));
        for(var token in tokens) {
            var replacement = tokens[token];
            token = regExpEscape(tokenPattern.replace('{token}', token));
            string = string.replace(new RegExp(token, 'g'), replacement);
        }
        return string;
    };
    window.Riddlemd.TemplateTools.replaceTokensInString = replaceTokensInString;
}(window));