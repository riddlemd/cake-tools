(function(window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.nullCoalesce = function(value, coalesce) {
        return value === null || typeof value === 'undefined' ? coalesce : value;
    };
}(window));