// Fullscreen mode
(function(window) {
    window.isFullscreen = function() {
        return window.document.fullscreenElement || window.document.mozFullScreenElement || window.document.webkitFullscreenElement || window.document.msFullscreenElement ? true : false;
    };
    window.requestFullscreen = function() {
        var func = window.document.documentElement.requestFullscreen || window.document.documentElement.mozRequestFullScreen || window.document.documentElement.webkitRequestFullScreen || window.document.documentElement.msRequestFullscreen;
        func.call(window.document.documentElement);
    };
    window.cancelFullscreen = function() {
        var func = window.document.exitFullscreen || window.document.mozCancelFullScreen || window.document.webkitExitFullscreen || window.document.msExitFullscreen;
        func.call(window.document.documentElement);
    };
    window.toggleFullscreen = function() {
        if(window.isFullscreen()) {
            window.cancelFullscreen();
        }
        else {
            window.requestFullscreen();
        }
    };
}(window));