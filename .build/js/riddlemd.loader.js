(function(window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.Loader = function Loader() {
        var currentLoader = this;
        var queue = [];
        var triggeredKeys = [];

        this.deferScript = function(input, options) {
            options = Object.assign({
                key: undefined,
                dependencyKey: undefined,
                attributes: undefined
            }, options);
            options.data = input;

            if(window.document.readyState !== 'complete' || (options.dependencyKey && triggeredKeys.indexOf(options.dependencyKey) == -1)) {
                queue.push(options);
            }
            else {
                processDeferedScript(options);
            }
            return this;
        }

        function processDeferedStyles() {
            var deferedStyleWrappers = document.getElementsByClassName('defered-styles');

            for(var i = 0; i < deferedStyleWrappers.length; i++) {
                var deferedStyleWrapper = deferedStyleWrappers[i];
                var div = window.document.createElement('div');
                div.innerHTML = deferedStyleWrapper.innerText;
                document.getElementsByTagName('head')[0].append(div);

                deferedStyleWrapper.parentElement.removeChild(deferedStyleWrapper);
            };
        }

        function processDeferedScript(deferedScript) {
            var type = typeof deferedScript.data;

            if(type == 'function') {
                deferedScript.data();
                if(deferedScript.key) {
                    processDeferedScripts(deferedScript.key);
                }
            } else if(type == 'string') {
                var elm = document.createElement('script');
                elm.src = deferedScript.data;
                
                if(deferedScript.key) {
                    elm.setAttribute('data-key', deferedScript.key);
                }

                elm.addEventListener('load', function() {
                    var key = this.getAttribute('data-key');
                    if(key) {
                        processDeferedScripts(key);
                    }
                });

                if(deferedScript.attributes) {
                    for(let attribute in deferedScript.attributes) {
                        elm.setAttribute(attribute, deferedScript.attributes[attribute]);
                    }
                }

                document.getElementsByTagName('head')[0].appendChild(elm);
            }
        }

        function processDeferedScripts(dependencyKey) {
            triggeredKeys.push(dependencyKey);
            dependencyKey = dependencyKey || null;
            queue = queue.filter(function(deferedScript, index) {
                if(deferedScript.dependencyKey == dependencyKey) {
                    processDeferedScript(deferedScript)
                    return false;
                }
                return true;
            });
        }

        window.addEventListener('load', function(e) {
            processDeferedScripts();
            processDeferedStyles();
        });
    };
}(window));