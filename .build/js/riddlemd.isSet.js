(function(window) {
    if(typeof window.Riddlemd !== 'object') window.Riddlemd = {};

    window.Riddlemd.isSet = function(obj) {
        return typeof obj !== 'undefined' && obj !== null;
    };
})(window);